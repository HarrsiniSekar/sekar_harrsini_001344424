/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import Business.Employee.Employee;
import Business.Organization.AdminOrganization;
import Business.Organization.DoctorOrganization;
import Business.Organization.LabOrganization;
import Business.UserAccount.UserAccount;

/**
 *
 * @author ran
 */
public class ConfigureABusiness {
    
    public static Business configure(){
        // Three roles: LabAssistant, Doctor, Admin
        
        Business business = Business.getInstance();
        
        
        AdminOrganization adminorganization = new AdminOrganization();
        business.getOrganizationDirectory().getOrganizationList().add(adminorganization);
        Employee employee = new Employee();
        employee.setName("Harrsini");
        UserAccount useraccount = new UserAccount();
        useraccount.setUsername("admin");
        useraccount.setPassword("admin");
        useraccount.setRole("Admin");
        useraccount.setEmployee(employee);
        adminorganization.getEmployeeDirectory().getEmployeeList().add(employee);
        adminorganization.getUserAccountDirectory().getUserAccountList().add(useraccount);
        

        
        DoctorOrganization doctororganization = new DoctorOrganization();
        business.getOrganizationDirectory().getOrganizationList().add(doctororganization);
        Employee employee1 = new Employee();
        employee1.setName("Jim");
        UserAccount useraccount1 = new UserAccount();
        useraccount1.setUsername("doctor");
        useraccount1.setPassword("doctor");
        useraccount1.setRole("Doctor");
        useraccount1.setEmployee(employee1);
        doctororganization.getEmployeeDirectory().getEmployeeList().add(employee1);
        doctororganization.getUserAccountDirectory().getUserAccountList().add(useraccount1);
 
        
        LabOrganization laborganization = new LabOrganization();
        business.getOrganizationDirectory().getOrganizationList().add(laborganization);
        Employee employee2 = new Employee();
        employee2.setName("Jack");
        UserAccount useraccount2 = new UserAccount();
        useraccount2.setUsername("lab");
        useraccount2.setPassword("lab");
        useraccount2.setRole("LabAssistant");
        useraccount2.setEmployee(employee2);
        laborganization.getEmployeeDirectory().getEmployeeList().add(employee2);
        laborganization.getUserAccountDirectory().getUserAccountList().add(useraccount2);

      
        return business;
        
    }
    
}
