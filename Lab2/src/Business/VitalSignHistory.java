/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Harrsini Sekar
 */
public class VitalSignHistory {
    
    private ArrayList<VitalSign> vitalSignHistory;

    public VitalSignHistory()
    {
          vitalSignHistory = new ArrayList<VitalSign>();
    
    }
    
    public ArrayList<VitalSign> getVitalsignhistory() {
        return vitalSignHistory;
    }

    public void setVitalsignhistory(ArrayList<VitalSign> vitalSignHistory) {
        this.vitalSignHistory = vitalSignHistory;
    }
    
    public VitalSign addvitals()
    {
        VitalSign vs = new VitalSign();
        vitalSignHistory.add(vs);
        return vs; 
    }
    
    public void deletevitals(VitalSign v)
    {
       vitalSignHistory.remove(v);
    }
     
}
