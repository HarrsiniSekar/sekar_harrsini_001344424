/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface;

import Business.VitalSign;
import Business.VitalSignHistory;
import javax.swing.JOptionPane;

/** 
 *
 * @author Harrsini Sekar
 */
public class CreateVitalsJPanel extends javax.swing.JPanel {

    /**
     * Creates new form CreateVitalsJPanel
     */
 
    private VitalSignHistory vsh;
    public CreateVitalsJPanel(VitalSignHistory vsh) {
        initComponents(); 
        this.vsh = vsh;
        
//To change body of generated methods, choose Tools | Templates.
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        createVitalSignLabel = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        SubmitBtn = new javax.swing.JButton();
        TemperatureTextField = new javax.swing.JTextField();
        BloodPressureTextField = new javax.swing.JTextField();
        PulseTextField = new javax.swing.JTextField();
        DateTextField = new javax.swing.JTextField();

        createVitalSignLabel.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        createVitalSignLabel.setText("Create Vital Signs");

        jLabel2.setText("Temperature :");

        jLabel3.setText("Blood Pressure :");

        jLabel4.setText("Pulse :");

        jLabel5.setText("Date :");

        SubmitBtn.setText("SUBMIT");
        SubmitBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SubmitBtnActionPerformed(evt);
            }
        });

        TemperatureTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TemperatureTextFieldActionPerformed(evt);
            }
        });

        BloodPressureTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BloodPressureTextFieldActionPerformed(evt);
            }
        });

        PulseTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                PulseTextFieldActionPerformed(evt);
            }
        });

        DateTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DateTextFieldActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(55, 55, 55)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3)
                            .addComponent(jLabel4)
                            .addComponent(jLabel5))
                        .addGap(82, 82, 82)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(TemperatureTextField, javax.swing.GroupLayout.DEFAULT_SIZE, 268, Short.MAX_VALUE)
                            .addComponent(BloodPressureTextField)
                            .addComponent(PulseTextField)
                            .addComponent(DateTextField)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(43, 43, 43)
                        .addComponent(createVitalSignLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 366, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 591, Short.MAX_VALUE)
                .addComponent(SubmitBtn)
                .addGap(338, 338, 338))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addComponent(createVitalSignLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(35, 35, 35)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(TemperatureTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(39, 39, 39)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(BloodPressureTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(39, 39, 39)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(PulseTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(41, 41, 41)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(DateTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(103, 103, 103)
                .addComponent(SubmitBtn)
                .addContainerGap(234, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void BloodPressureTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BloodPressureTextFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_BloodPressureTextFieldActionPerformed

    private void PulseTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_PulseTextFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_PulseTextFieldActionPerformed

    private void DateTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DateTextFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_DateTextFieldActionPerformed

    private void TemperatureTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TemperatureTextFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_TemperatureTextFieldActionPerformed

    private void SubmitBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SubmitBtnActionPerformed
        // TODO add your handling code here:
        double Temperature = Double.parseDouble(TemperatureTextField.getText());
        
        double BloodPressure = Double.parseDouble(BloodPressureTextField.getText());
        
        int Pulse = Integer.parseInt(PulseTextField.getText());
        
        String Date = DateTextField.getText();
        
        VitalSign v = vsh.addvitals();
        v.setBloodPressure(BloodPressure);
        v.setDate(Date);
        v.setPulse(Pulse);
        v.setTemperature(Temperature);
        
        
        JOptionPane.showMessageDialog(null, "Vital Signs Added Successfully");
        TemperatureTextField.setText("");
        BloodPressureTextField.setText("");
        PulseTextField.setText("");
        DateTextField.setText("");
        
        
        
    }//GEN-LAST:event_SubmitBtnActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField BloodPressureTextField;
    private javax.swing.JTextField DateTextField;
    private javax.swing.JTextField PulseTextField;
    private javax.swing.JButton SubmitBtn;
    private javax.swing.JTextField TemperatureTextField;
    private javax.swing.JLabel createVitalSignLabel;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    // End of variables declaration//GEN-END:variables
}
