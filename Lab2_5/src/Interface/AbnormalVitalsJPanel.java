/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface;
import Business.VitalSignHistory;
import Business.VitalSign;
import javax.swing.JOptionPane;
import Interface.ViewVitalsJPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Harrsini Sekar
 */
public class AbnormalVitalsJPanel extends javax.swing.JPanel {

    /**
     * Creates new form AbnormalVitalsJPanel
     */
    private VitalSignHistory vsh;
    private double minbp;
    private double maxbp;
    public AbnormalVitalsJPanel(VitalSignHistory vsh,double maxbp,double minbp) {
        initComponents();
        this.vsh=vsh;
        this.maxbp=maxbp;
        this.minbp=minbp;
        populateTable();
    }

    

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        ViewTable = new javax.swing.JTable();
        ViewBtn = new javax.swing.JButton();
        DeleteBtn = new javax.swing.JButton();
        updateBtn = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        TemperatureTextField = new javax.swing.JTextField();
        BloodPressureTextField = new javax.swing.JTextField();
        PulseTextField = new javax.swing.JTextField();
        DateTextField = new javax.swing.JTextField();
        ConfirmBtn = new javax.swing.JButton();

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        jLabel1.setText("Vital Signs");

        ViewTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Date", "Blood Pressure"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(ViewTable);

        ViewBtn.setText("View Details");
        ViewBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ViewBtnActionPerformed(evt);
            }
        });

        DeleteBtn.setText("Delete");
        DeleteBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DeleteBtnActionPerformed(evt);
            }
        });

        updateBtn.setText("Update");
        updateBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                updateBtnActionPerformed(evt);
            }
        });

        jLabel2.setText("Temperature :");

        jLabel3.setText("Blood Pressure :");

        jLabel4.setText("Pulse :");

        jLabel5.setText("Date :");

        TemperatureTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TemperatureTextFieldActionPerformed(evt);
            }
        });

        BloodPressureTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BloodPressureTextFieldActionPerformed(evt);
            }
        });

        PulseTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                PulseTextFieldActionPerformed(evt);
            }
        });

        DateTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DateTextFieldActionPerformed(evt);
            }
        });

        ConfirmBtn.setText("Confirm");
        ConfirmBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ConfirmBtnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(86, 86, 86)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 217, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 474, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(83, 83, 83)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(updateBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(DeleteBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(60, 60, 60)
                                .addComponent(ViewBtn)))))
                .addContainerGap(180, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addGap(107, 107, 107)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(jLabel5)
                        .addComponent(jLabel4)))
                .addGap(82, 82, 82)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(TemperatureTextField)
                    .addComponent(BloodPressureTextField)
                    .addComponent(PulseTextField)
                    .addComponent(DateTextField, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 268, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(ConfirmBtn)
                .addGap(56, 56, 56))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(46, 46, 46)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(14, 14, 14)
                        .addComponent(ViewBtn)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(updateBtn)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(DeleteBtn))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 196, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(118, 118, 118)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addComponent(TemperatureTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(29, 29, 29)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(BloodPressureTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(44, 44, 44)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(PulseTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(28, 28, 28)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(DateTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap(242, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(ConfirmBtn)
                        .addGap(216, 216, 216))))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void ViewBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ViewBtnActionPerformed
        // TODO add your handling code here:
        int selectedrow = ViewTable.getSelectedRow();

        if (selectedrow >= 0)
        {
            VitalSign vs = (VitalSign)ViewTable.getValueAt(selectedrow,0);
            BloodPressureTextField.setText(String.valueOf(vs.getBloodPressure()));
            TemperatureTextField.setText(String.valueOf(vs.getTemperature()));
            PulseTextField.setText(String.valueOf(vs.getPulse()));
            DateTextField.setText(vs.getDate());
        }
        else
        JOptionPane.showMessageDialog(null, "Please select row");
        
        setFieldEnabled(false);
    }//GEN-LAST:event_ViewBtnActionPerformed

    private void DeleteBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DeleteBtnActionPerformed
        // TODO add your handling code here:

        int selectedrow = ViewTable.getSelectedRow();

        if (selectedrow >= 0)
        {
            VitalSign vs = (VitalSign)ViewTable.getValueAt(selectedrow,0);
            vsh.deletevitals(vs);
            JOptionPane.showMessageDialog(null, "Deleted successfully");
            populateTable();
        }
        else
        JOptionPane.showMessageDialog(null, "Please select row");
    }//GEN-LAST:event_DeleteBtnActionPerformed

     public void populateTable()
    {
        
        DefaultTableModel dtm = (DefaultTableModel)ViewTable.getModel();
        dtm.setRowCount(0);
        for ( VitalSign vs : vsh.getAbnormalList(maxbp,minbp)){
        {
            Object row[] = new Object[2];
            row[0] = vs;
            row[1] = vs.getBloodPressure();
            dtm.addRow(row);
                 
        }
}
    }
     
    private void updateBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_updateBtnActionPerformed
        // TODO add your handling code here:
        setFieldEnabled(true);
        //updateBtn.setEnabled(true);

        int selectedrow = ViewTable.getSelectedRow();

        if (selectedrow >= 0)
        {
            VitalSign vs = (VitalSign)ViewTable.getValueAt(selectedrow,0);
            TemperatureTextField.setText(Double.toString(vs.getTemperature()));
            PulseTextField.setText(String.valueOf(vs.getPulse()));
            BloodPressureTextField.setText(String.valueOf(vs.getBloodPressure()));
            DateTextField.setText(vs.getDate());

        }
        else
        JOptionPane.showMessageDialog(null, "Please select row");

    }//GEN-LAST:event_updateBtnActionPerformed

     private void setFieldEnabled(boolean b)
    {
            TemperatureTextField.setEnabled(b);
            BloodPressureTextField.setEnabled(b);
            PulseTextField.setEnabled(b);
            DateTextField.setEnabled(b);
    }
    private void TemperatureTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TemperatureTextFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_TemperatureTextFieldActionPerformed

    private void BloodPressureTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BloodPressureTextFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_BloodPressureTextFieldActionPerformed

    private void PulseTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_PulseTextFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_PulseTextFieldActionPerformed

    private void DateTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DateTextFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_DateTextFieldActionPerformed

    private void ConfirmBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ConfirmBtnActionPerformed
        // TODO add your handling code here:
        ConfirmBtn.setEnabled(true);
        int selectedrow = ViewTable.getSelectedRow();

        if (selectedrow >= 0)
        {
            VitalSign vs = (VitalSign)ViewTable.getValueAt(selectedrow,0);
            vs.setTemperature(Double.parseDouble(TemperatureTextField.getText()));
            vs.setBloodPressure(Double.parseDouble(BloodPressureTextField.getText()));
            vs.setPulse(Integer.parseInt(PulseTextField.getText()));
            vs.setDate(DateTextField.getText());
            JOptionPane.showMessageDialog(null, "Vital Signs Successfully updated");
            populateTable();
            ConfirmBtn.setEnabled(true);
            setFieldEnabled(true);

        }
        else
        JOptionPane.showMessageDialog(null, "Please select row");

    }//GEN-LAST:event_ConfirmBtnActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField BloodPressureTextField;
    private javax.swing.JButton ConfirmBtn;
    private javax.swing.JTextField DateTextField;
    private javax.swing.JButton DeleteBtn;
    private javax.swing.JTextField PulseTextField;
    private javax.swing.JTextField TemperatureTextField;
    private javax.swing.JButton ViewBtn;
    private javax.swing.JTable ViewTable;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton updateBtn;
    // End of variables declaration//GEN-END:variables
}
