/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Harrsini Sekar
 */
public class VitalSignHistory {
    
    private ArrayList<VitalSign> vitalSignHistory;

    public VitalSignHistory()
    {
          vitalSignHistory = new ArrayList<VitalSign>();
    
    }
    
    public ArrayList<VitalSign> getVitalsignhistory() {
        return vitalSignHistory;
    }

    public void setVitalsignhistory(ArrayList<VitalSign> vitalSignHistory) {
        this.vitalSignHistory = vitalSignHistory;
    }
    
    public VitalSign addvitals()
    {
        VitalSign vs = new VitalSign();
        vitalSignHistory.add(vs);
        return vs; 
    }
    
    public void deletevitals(VitalSign v)
    {
       vitalSignHistory.remove(v);
    }
    
     public List<VitalSign> getAbnormalList(double maxbp, double minbp)
    {
        List<VitalSign> abnList = new ArrayList<>();
        
        for(VitalSign vs: vitalSignHistory)
        {  
         if(vs.getBloodPressure()> maxbp || vs.getBloodPressure()< minbp) {
         abnList.add(vs);
         }
        }
          return abnList;
    }
     
}
